const List playlist = [
  {
    "img": "assets/images/Top 100.png",
    "title": "Billboard Hot 100",
    "description":"The official  features this week's most popular songs across all genres",

    "songs": [
      {"title": "First Class", "artist":"Jack Halow","duration": "1:21","song_img":"assets/song_images/firstclass.png"},
      {"title": "As It Was", "artist":"Harry Style","duration": "2:17","song_img":"assets/song_images/asitwas.png"},
      {"title": "WAIT FOR U", "artist":"Future,Drake,Tems","duration": "4:21","song_img":"assets/song_images/waitforu.png"},
      {"title": "Moscow Mule", "artist":"Bad Bunny","duration": "3:00","song_img":"assets/song_images/moscowmule.png"},
      {"title": "Heat Waves", "artist":"Glass Animal","duration": "5:50","song_img":"assets/song_images/heatwaves.png"},
      {"title": "Big Energy", "artist":"Latto","duration": "2:45","song_img":"assets/song_images/bigenergy.png"},
      {"title": "About Damn Time", "artist":"Lizzo","duration": "3:11","song_img":"assets/song_images/aboutdamntime.png"},
    ]
  },
  {
    "img": "assets/images/Top 100_vietnam.png",
    "title": "Top 100 Việt Nam",
    "description":"These are the most popular songs in Vietnam",
    "songs": [
      {"title": "With you", "artist":"Jimin, HA SUNG WOON","duration": "5:12","song_img":"assets/song_images/withyou.png"},
      {"title": "Có không giữ mất đừng tìm", "artist":"Trúc Nhân","duration": "2:17","song_img":"assets/song_images/cokhonggiumatdungtim.png"},
      {"title": "THERE'S NO ONE AT ALL", "artist":"Sơn Tùng MTP","duration": "4:21","song_img":"assets/song_images/theresnooneatall.png"},
      {"title": "Ánh sao và bầu trời", "artist":"T.R.I","duration": "3:00","song_img":"assets/song_images/anhsaovabautroi.png"},
      {"title": "Butter", "artist":"BTS","duration": "5:50","song_img":"assets/song_images/butter.png"},
      {"title": "Có em", "artist":"Madihu,LowG","duration": "2:45","song_img":"assets/song_images/coem.png"},
      {"title": "Chìm sâu", "artist":"MCK, Trung Trần","duration": "3:11","song_img":"assets/song_images/chimsau.png"},
    ]

  },
  {
    "img": "assets/images/rapviet.png",
    "title": "RapViet playlist",
    "description":"RapViet channel's hot hits",
    "songs": [
      {"title": "Va vào giai điệu này", "artist":"RPT MCK","duration": "3:21","song_img":"assets/song_images/rapviet1.png"},
      {"title": "Dân Chơi Xóm", "artist":"RPT MCK, JayTee","duration": "2:17","song_img":"assets/song_images/rapviet1.png"},
      {"title": "101520", "artist":"Sol7, PrettyXIX","duration": "4:21","song_img":"assets/song_images/rapviet2.png"},
      {"title": "Cảm nhận", "artist":"Seachains","duration": "3:00","song_img":"assets/song_images/rapviet2.png"},
      {"title": "Back to hometown", "artist":"Sol7","duration": "5:50","song_img":"assets/song_images/rapviet2.png"},
      {"title": "Tâm trong tim", "artist":"Wowy, Blacka","duration": "2:45","song_img":"assets/song_images/rapviet2.png"},
      {"title": "Khắc cốt ghi tâm", "artist":"Seachains, Dlow, Karik","duration": "3:11","song_img":"assets/song_images/rapviet2.png"},
    ]

  },
  {
    "img": "assets/images/chill_hits.png",
    "title": "Chill hits playlist",
    "description":"Kick back to the best new and recent chill hits",
    "songs": [
      {"title": "At My Worst", "artist":"Pink Sweat","duration": "1:21","song_img":"assets/song_images/atmyworst.png"},
      {"title": "Cheating on You", "artist":"Charlie Puth","duration": "2:17","song_img":"assets/song_images/cheatingonyou.png"},
      {"title": "Its OK If You Forget Me", "artist":"Astrid S","duration": "4:21","song_img":"assets/song_images/itsokifyouforgetmet.png"},
      {"title": "Airplan Thoughts", "artist":"dhruv","duration": "3:00","song_img":"assets/song_images/airplanethought.png"},
      {"title": "Are you Happy", "artist":"SHY Martin","duration": "5:50","song_img":"assets/song_images/areyouhappy.png"},
      {"title": "colorblind", "artist":"Mokita","duration": "2:45","song_img":"assets/song_images/colorblind.png"},
      {"title": "About Damn Time", "artist":"Lizzo","duration": "3:11","song_img":"assets/song_images/aboutdamntime.png"},
    ]

  },
  {
    "img": "assets/images/daily_mix.png",
    "title": "Daily mix",
    "description":"Playlist just for you ",
    "songs": [
      {"title": "abcdefu", "artist":"GAYLE","duration": "1:21","song_img":"assets/song_images/abcdefu.png"},
      {"title": "At My Worst", "artist":"Pink Sweat","duration": "2:17","song_img":"assets/song_images/atmyworst.png"},
      {"title": "good 4 u", "artist":"Olivia Rodrigo","duration": "4:21","song_img":"assets/song_images/good4u.png"},
      {"title": "Người Ấy", "artist":"Trinh Thang Binh","duration": "3:00","song_img":"assets/song_images/nguoiay.png"},
      {"title": "Someone You Loved", "artist":"Lewis Capaldi","duration": "5:50","song_img":"assets/song_images/someoneyoulove.png"},
      {"title": "Ngày Mai Em Đi", "artist":"Lê Hiếu,Soobin Hoang Son,Touliver","duration": "2:45","song_img":"assets/song_images/ngaymaiemdi.png"},
      {"title": "Levitating (feat. DaBaby)", "artist":"DaBabyDua, Lipa","duration": "3:11","song_img":"assets/song_images/levitating.png"},
    ]

  },
  {
    "img": "assets/images/sadsong.png",
    "title": "Sad Songs",
    "description":"Beautiful songs to break your heart...",
    "songs": [
      {"title": "lovely (with Khalid)", "artist":"Billie EilishKhalid","duration": "1:21","song_img":"assets/song_images/lovely.png"},
      {"title": "7 Years", "artist":"Lukas Graham","duration": "2:17","song_img":"assets/song_images/7years.png"},
      {"title": "Another Love", "artist":"Tom Odell","duration": "4:21","song_img":"assets/song_images/anotherlove.png"},
      {"title": "Easy On Me", "artist":"Adele","duration": "3:00","song_img":"assets/song_images/easyonme.png"},
      {"title": "Sign of the Times", "artist":"Harry Styles","duration": "5:50","song_img":"assets/song_images/signoftime.png"},
      {"title": "drivers license", "artist":"Olivia Rodrigo","duration": "2:45","song_img":"assets/song_images/driverslicense.png"},
      {"title": "See You Again (feat. Charlie Puth)", "artist":"Charlie Puth, Wiz Khalifa","duration": "3:11","song_img":"assets/song_images/seeyouagain.png"},
    ]

  },
  {
    "img": "assets/images/indieviet.png",
    "title": "Indie Việt",
    "description":"Những ca khúc hay nhất từ dòng nhạc mới mẻ và đa dạng.",
    "songs": [
      {"title": "Bao Tiền Một Mớ Bình Yên", "artist":"14 Casper","duration": "1:21","song_img":"assets/song_images/baotienmotmotinhyeu.png"},
      {"title": "Internet Love", "artist":"VSTRA","duration": "2:17","song_img":"assets/song_images/internetlove.png"},
      {"title": "3 1 0 7", "artist":"Duong, Nâu, W/N","duration": "4:21","song_img":"assets/song_images/3107.png"},
      {"title": "Một Mình Ta", "artist":"buitruonglinh","duration": "3:00","song_img":"assets/song_images/motminhta.png"},
      {"title": "Tiny Love", "artist":"Thịnh Suy","duration": "5:50","song_img":"assets/song_images/tinylove.png"},
      {"title": "Ánh Sao Và Bầu Trời", "artist":"T.R.I","duration": "2:45","song_img":"assets/song_images/anhsaovabautroi.png"},
      {"title": "Bước Qua Nhau", "artist":"Vũ","duration": "3:11","song_img":"assets/song_images/buocquanhau.png"},
    ]

  },
  {
    "img": "assets/images/softpophits.png",
    "title": "SOFT POP HITS 2022",
    "description":"The BEST Soft Pop Hits playlist on Spotify!",
    "songs": [
      {"title": "Ghost", "artist":"Justin Bieber","duration": "1:21","song_img":"assets/song_images/ghost.png"},
      {"title": "I'm Not The Only One", "artist":"Sam Smith","duration": "2:17","song_img":"assets/song_images/imnottheonlyone.png"},
      {"title": "Heartbreak Anniversary", "artist":"Giveon","duration": "4:21","song_img":"assets/song_images/heartbreakanniversary.png"},
      {"title": "Lover", "artist":"Taylor Swift","duration": "3:00","song_img":"assets/song_images/lover.png"},
      {"title": "you broke me first", "artist":"Tate McRae","duration": "5:50","song_img":"assets/song_images/youbrokemefirst.png"},
      {"title": "Butterflies", "artist":"Ali Gatie,MAX","duration": "2:45","song_img":"assets/song_images/butterflies.png"},
      ]

  },
  {
    "img": "assets/images/workout.png",
    "title": "Workout music",
    "description":"Pop hits to keep your workout fresh.",
    "songs": [
      {"title": "Gimme! Gimme! Gimme!", "artist":"HÄWK","duration": "1:21","song_img":"assets/song_images/gimme.png"},
      {"title": "The Motto", "artist":"Ava Max, Tiësto","duration": "2:17","song_img":"assets/song_images/themotto.png"},
      {"title": "goosebumps", "artist":"Arem Ozguc, Arman Aydin, Jordan Rys, OH, HAYIR!","duration": "4:21","song_img":"assets/song_images/goosebumps.png"},
      {"title": "Do It To It", "artist":"ACRAZE, Cherish","duration": "3:00","song_img":"assets/song_images/doittoit.png"},
      {"title": "Clap Your Hands", "artist":"Kungs","duration": "5:50","song_img":"assets/song_images/clapyourhands.png"},
      {"title": "Pop Off", "artist":"Fabian Mazur","duration": "2:45","song_img":"assets/song_images/popoff.png"},
      ]

  },
  {
    "img": "assets/images/deephouse.png",
    "title": "Deep House Relax",
    "description":"Forget it and disapear with chill house",
    "songs": [
      {"title": "The Motto", "artist":"Ava Max, Tiësto","duration": "2:17","song_img":"assets/song_images/themotto.png"},
      {"title": "Don't Forget My Love", "artist":"Diplo, Miguel","duration": "2:17","song_img":"assets/song_images/dontforgetmylove.png"},
      {"title": "Tell Me", "artist":"Hanne Mjøen, James Carters","duration": "4:21","song_img":"assets/song_images/tellme.png"},
      {"title": "Last Time", "artist":"Amadea, yuma.","duration": "3:00","song_img":"assets/song_images/lasttime.png"},
      {"title": "Body on Fire", "artist":"Dillistone, SOMMA","duration": "5:50","song_img":"assets/song_images/bodyonfire.png"},
      {"title": "Smalltown Boy", "artist":"Indiana,Marcus Layton","duration": "2:45","song_img":"assets/song_images/smalltownboy.png"},
      {"title": "About Damn Time", "artist":"Lizzo","duration": "3:11","song_img":"assets/song_images/aboutdamntime.png"},
    ]

  },
];
