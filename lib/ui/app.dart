import 'package:flutter/material.dart';
import 'login_screen.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'xLMS',
      theme: ThemeData(
          primarySwatch: Colors.indigo
      ),
      home: LoginScreen(),
    );
  }

}
