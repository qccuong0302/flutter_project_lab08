import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../data/data_json.dart';
class LibraryPage extends StatefulWidget {
  const LibraryPage({Key? key}) : super(key: key);

  @override
  State<LibraryPage> createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Library",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),),
        actions: [
          IconButton(onPressed: (){}, icon: Icon(Icons.search)),
          IconButton(onPressed: (){}, icon: Icon(CupertinoIcons.plus))
        ],
      ),
      body: Body(),

    );
  }

 Widget Body() {
    return Container(
      alignment: Alignment.bottomCenter,
      child: ListView.separated(

        padding: const EdgeInsets.all(20),
        itemCount: playlist.length,
        itemBuilder: (context,index){
          return ListTile(

            leading: CircleAvatar(backgroundImage: AssetImage(playlist[index]['img']),),
            title: Text(playlist[index]['title'],style:TextStyle(color: Colors.white,fontSize: 20)),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return Divider(color: Colors.white,);
        },
      ),
    );





 }
  
}
