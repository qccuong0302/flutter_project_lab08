import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import '../data/data_json.dart';
import 'playlist.dart';

class HomeBody extends StatefulWidget {
  const HomeBody({Key? key}) : super(key: key);

  @override
  State<HomeBody> createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  int selectType = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text("Spotify ", style: TextStyle(fontSize: 30,
                color: Colors.green,
                fontWeight: FontWeight.bold)),
            IconButton(onPressed: () {
              Navigator.push(context,MaterialPageRoute(builder:(context)=>const NotificationPage()));
            },
                icon: Icon(Icons.notifications, color: Colors.white,)),
            IconButton(onPressed: () {
              Navigator.push(context,MaterialPageRoute(builder:(context)=>const RecentPage()));
            },
                icon: Icon(Icons.alarm, color: Colors.white,)),
            IconButton(onPressed: () {
              Navigator.push(context,MaterialPageRoute(builder:(context)=>const SettingPage()));
            },
                icon: Icon(Icons.settings, color: Colors.white,))
          ],
        ),
      ),
      body: homeBody(),
    );
  }

  Widget homeBody() {
    List song_type = [
      "Music", "Rap & Hip Hop", "Pop", "Podcast", "Channel", "R&B", "Soul"
    ];
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Hi, User ♥", style: TextStyle(color: Colors.white,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Padding(
                  padding: const EdgeInsets.only(left: 30, top: 20),
                  child: Row(
                    children:
                    List.generate(song_type.length, (index) {
                      return Padding(
                        padding: const EdgeInsets.only(right: 25),
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              selectType = index;
                            });
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                song_type[index],
                                style: TextStyle(
                                    fontSize: 15,
                                    color: selectType == index ? Colors.green : Colors.grey,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              selectType == index
                                  ? Container(
                                width: 10,
                                height: 3,
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.circular(5)),
                              )
                                  : Container()
                            ],
                          ),
                        ),
                      );
                    }),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Row(
                        children: List.generate(5, (index) {
                          return Padding(
                            padding: const EdgeInsets.only(right: 30),
                          child: GestureDetector(
                            onTap: () {
                            Navigator.push(context,PageTransition(
                              alignment: Alignment.bottomCenter,
                              child: PlaylistPage(
                              song: playlist[index],
                          ),
                          type: PageTransitionType.scale));
                          },
                            child: Column(
                                children: [Container(
                                  width: 180,
                                  height: 180,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(playlist[index]['img']),
                                        fit: BoxFit.cover),
                                    color: Colors.green,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    playlist[index]['title'],
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    width: 180,
                                    child: Text(
                                      playlist[index]['description'],
                                      maxLines: 1,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  )
                                ]
                            ),
                          )
                          );
                        }),
                      )
                  )
              )
            ],
          ),
          SizedBox(height: 20,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Row(
                        children: List.generate(5, (index) {
                          return Padding(
                              padding: const EdgeInsets.only(right: 30),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(context,PageTransition(
                                      alignment: Alignment.bottomCenter,
                                      child: PlaylistPage(
                                        song: playlist[index+5],
                                      ),
                                      type: PageTransitionType.scale));
                                },
                                child: Column(
                                    children: [Container(
                                      width: 180,
                                      height: 180,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(playlist[index+5]['img']),
                                            fit: BoxFit.cover),
                                        color: Colors.green,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        playlist[index+5]['title'],
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        width: 180,
                                        child: Text(
                                          playlist[index+5]['description'],
                                          maxLines: 1,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      )
                                    ]
                                ),
                              )
                          );
                        }),
                      )
                  )
              )
            ],
          ),

        ],
      ),

    );
  }
}
class NotificationPage extends StatelessWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(backgroundColor: Colors.black,),
      body:Center(
          child: const Text("Notification Page",style: TextStyle(color: Colors.white,fontSize: 50),),

      )
    );

  }
}
class RecentPage extends StatelessWidget {
  const RecentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(backgroundColor: Colors.black,),
        body:Center(
          child: const Text("Recent Page",style: TextStyle(color: Colors.white,fontSize: 50),),

        )
    );
  }
}
class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(backgroundColor: Colors.black,),
        body:Center(
          child: const Text("Setting Page",style: TextStyle(color: Colors.white,fontSize: 50),),

        )
    );
  }
}

