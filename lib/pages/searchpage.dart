import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Search"),
        actions: [
          IconButton(onPressed: (){

          }, icon: Icon(Icons.camera_alt_outlined))
        ],
      ),
      body: Container(

          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10,right: 10,bottom: 20,),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:  BorderRadius.circular(32),
                  ),
                  child: TextField(
                    decoration: InputDecoration(
                      hintStyle: TextStyle(fontSize: 17),
                      hintText: 'Search artist,podcast,songs,...',
                      suffixIcon: Icon(Icons.search),
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.all(20),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 50,),
              Padding(
                padding: const EdgeInsets.only(top: 50),
                child: GridView.count(
                  childAspectRatio: MediaQuery.of(context).size.height/300,
                  primary: false,
                  padding: const EdgeInsets.only(top: 20),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 2,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.redAccent,),
                      child: Center(
                        child: const Text("Rock",textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                      )
                    ),
                    Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.teal,),
                      child:Center(
                        child:
                      const Text('Rap',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                      )
                    ),
                    Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.purpleAccent,),
                      child: Center(
                        child:const Text('For you',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                    )),
                    Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.greenAccent,),
                      child: Center(
                        child:const Text('Podcast',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                    )
                    ),
                    Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.brown,),
                        child: Center(
                        child:const Text('Radio',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                      )),
                    Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),  color: Colors.blueGrey,),
                      child: Center(
                        child:const Text('Health',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                    )),
                    Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),
                        color: Colors.pinkAccent,),
                      child:Center(
                        child:const Text('Ambient',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                    )),
                    Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.redAccent,),

                      child: Center(
                        child:const Text('Ranking',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                    )),
                    Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.orangeAccent,),

                        child: Center(
                          child:const Text('Live Event',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                        )),
                    Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.indigoAccent,),

                        child: Center(
                          child:const Text('Khám phá',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                        )),
                    Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.lightGreen,),

                        child: Center(
                          child:const Text('Tâm trạng',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                        )),
                    Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.tealAccent,),

                        child: Center(
                          child:const Text('Experiences EQUAL',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                        )),
                    Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.amber,),

                        child: Center(
                          child:const Text('EQUAL',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                        )),
                    Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.orangeAccent,),

                        child: Center(
                          child:const Text('Trending',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                        )),
                    Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.blueAccent,),

                        child: Center(
                          child:const Text('Indie',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                        )),
                    Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.deepPurpleAccent,),

                        child: Center(
                          child:const Text('Tiệc tùng',textAlign: TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 32,fontWeight: FontWeight.bold)),
                        )),




                  ],
                ),
              ),
            ],
          ),

        ),

    );
  }

}

class CustomSearch {
}
