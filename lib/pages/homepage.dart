import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'homebody.dart';
import 'searchpage.dart';
import 'librarypage.dart';
class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int activeTab =0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      bottomNavigationBar: getBottomBar(),
      body: Body(),
    );
  }
  Widget Body() {

    return IndexedStack(
      index: activeTab,
      children: [
        HomeBody(),
        SearchPage(),
        LibraryPage(),


      ],
    );
  }
 Widget getBottomBar() {
    List items = [
      Icons.home,
      Icons.search,
      Icons.calendar_view_week_outlined

    ];
    return Container(
      height: 60,
      decoration: BoxDecoration(color: Colors.black),
      child:Padding(
      padding: const EdgeInsets.only(left: 20,right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: List.generate(items.length, (index){
          return IconButton(
              icon:Icon(

                items[index],
                color: activeTab == index ? Colors.greenAccent : Colors.white,
                size: 30,
              ),
              onPressed: (){
                setState((){
                  activeTab = index;
                });
              });

        })),
      ),
    );
 }

}


