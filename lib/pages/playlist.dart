import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'songdetail.dart';
class PlaylistPage extends StatefulWidget {
  final dynamic song;

  const PlaylistPage({Key? key,this.song}) : super(key: key);

  @override
  State<PlaylistPage> createState() => _PlaylistPageState();
}

class _PlaylistPageState extends State<PlaylistPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: playlistBody(),
    );
  }

 Widget playlistBody() {
    var size = MediaQuery.of(context).size;
    List playlistSong = widget.song['songs'];
    return SingleChildScrollView(
      child: Stack(
        children: [
          SizedBox(height: 30,),
          Column(
            children: [
              Container(
                width: size.width,
                height:220 ,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(widget.song['img']),
                        fit: BoxFit.contain)),
                ),
              SizedBox(height: 50,),
              Row(
                children: [Text(widget.song['title'],
                    style:TextStyle(fontSize: 30,
                        fontWeight:FontWeight.bold
                        ,color: Colors.white)
                )
                ],
              ),
              Row(
                  children:[
                  IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.favorite,color: Colors.white,)),
                  IconButton(onPressed: () {},
                      icon: Icon(Icons.arrow_circle_down_outlined, color: Colors.white,size: 30,)),
                  IconButton(onPressed: () {},
                      icon: Icon(Icons.more_horiz, color: Colors.white,)),
                ],),
              SizedBox(height: 30,),
              Column(
                children:List.generate(playlistSong.length, (index) {
                  return Padding(
                    padding: const EdgeInsets.only(left:30,right: 30,bottom: 10),
                    child: GestureDetector(
                      onTap: (){
                        Navigator.push(context, PageTransition(
                            child: SongDetail(
                              title: playlistSong[index]['title'],
                              artist: playlistSong[index]['artist'],
                              img: playlistSong[index]['song_img'],

                            ),
                            type: PageTransitionType.scale));
                      },
                      child: Row(
                        children: [
                          Container(
                            width: (size.width-40) * 0.8,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                               Text("${index+1} " +playlistSong[index]['title'],
                                 style: TextStyle(color: Colors.white, fontSize:18),),
                               Text("    "+playlistSong[index]['artist'], style: TextStyle(color: Colors.grey,fontSize: 14,fontWeight: FontWeight.w600),)

                              ],
                          ),),
                          Container(
                            width: 50,
                            height: 50,
                            child: Row(
                              verticalDirection: VerticalDirection.down,
                              children: [
                                Text(
                                  playlistSong[index]['duration'],
                                  style: TextStyle(color: Colors.grey, fontSize: 16),
                                ),

                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
              },
              )
              )
            ],
          )
        ],
      ),
    );

 }
}
