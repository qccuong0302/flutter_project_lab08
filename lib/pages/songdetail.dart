import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
class SongDetail extends StatefulWidget {
  final String title;
  final String artist;
  final String img;



  const SongDetail({Key? key, required this.title, required this.artist, required this.img}) : super(key: key);

  @override
  State<SongDetail> createState() => _SongDetailState();
}

class _SongDetailState extends State<SongDetail> {
  double _currentValue = 20;
  bool isPlaying = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        actions: [
          IconButton(onPressed: (){}, icon: Icon(Icons.more_horiz,color: Colors.white,))
        ],
      ),
      body: musicBody(),
    );
  }

 Widget musicBody() {
   var size = MediaQuery.of(context).size;
   return SingleChildScrollView(
     child: Stack(
       children: [
         SizedBox(height: 30,),
         Column(
           children: [
             Padding(
               padding: const EdgeInsets.only(left: 30,right: 30,top: 20),
               child: Container(
                 width: size.width-60,
                 height: size.width-60,
                 decoration: BoxDecoration(
                     image: DecorationImage(
                         image: AssetImage(widget.img),
                         fit: BoxFit.cover),
                   borderRadius: BorderRadius.circular(15)
                 ),
               ),
             ),
             SizedBox(height: 20,),
             Padding  (
               padding: const EdgeInsets.only(left: 10, right: 10),
               child: Container(
                 width: size.width - 80,
                 height: 100,
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: [
                     Container(
                       width: 200,
                       child: Column(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           Text(
                             widget.title,
                             style: TextStyle(
                                 fontSize:20,
                                 color: Colors.white,
                                 fontWeight: FontWeight.bold),
                           ),
                           Container(
                             width: 200,

                             child: Text(
                               widget.artist,

                               textAlign: TextAlign.center,
                               style: TextStyle(
                                   fontSize: 16, color: Colors.white.withOpacity(0.5)),
                             ),
                           ),

                         ],
                       ),
                     ),
                     IconButton(onPressed: (){}, icon: Icon(Icons.favorite,color: Colors.white,)),
                   ],
                 ),
               ),
             ),
             Slider(
                 activeColor: Colors.white,
                 value: _currentValue,
                 min: 0,
                 max: 200,
                 onChanged: (value) {
                   setState(() {
                     _currentValue = value;
                   });

                 }),
             SizedBox(
               height: 20,
             ),
             Padding(
               padding: const EdgeInsets.only(left: 30, right: 30),
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: [
                   Text(
                     "0:00",
                     style: TextStyle(color: Colors.white, fontSize: 18),
                   ),
                   Text(
                     "4:20",
                     style: TextStyle(color: Colors.white,fontSize: 18),
                   ),
                 ],
               ),
             ),
             Padding(
               padding: const EdgeInsets.only(left: 20, right: 20),
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: [
                   IconButton(
                       icon: Icon(
                         Icons.shuffle,
                         color: Colors.white.withOpacity(0.8),
                         size: 25,
                       ),
                       onPressed: (){}),
                   IconButton(
                       icon: Icon(
                         Icons.skip_previous,
                         color: Colors.white.withOpacity(0.8),
                         size: 30,
                       ),
                       onPressed: (){}),
                   IconButton(

                       iconSize:50 ,
                       icon: Container(

                         decoration: BoxDecoration(
                           shape: BoxShape.circle,
                           color: Colors.white
                         ),
                         child: Center(
                           child: Icon(isPlaying?Icons.stop_sharp:Icons.arrow_right_outlined),
                         ),
                       ),
                     onPressed: (){
                       if (isPlaying) {
                         setState(() {
                           isPlaying = false;
                         });
                       } else {
                         setState(() {
                           isPlaying = true;
                         });}
                       },
                     ),
                   IconButton(
                       icon: Icon(
                         Icons.skip_next,
                         color: Colors.white.withOpacity(0.8),
                         size: 30,
                       ),
                       onPressed: (){}),
                   IconButton(
                       icon: Icon(
                         Icons.autorenew,
                         color: Colors.white.withOpacity(0.8),
                         size: 25,
                       ),
                       onPressed: (){}),
                 ],
               ),
             ),
             SizedBox(
               height: 25,
             ),

        ],
      )
       ],
    ),
   );
 }
}
